function runDelayBetween (delay) {
    let len = arguments.length;
    let interval;
    if(+delay) 
      {
       interval = delay * 1000;
      } else throw(console.error('Only "Numbers" can bee delay variables!'));
    if (len < 2) throw(console.error('No function in queye!'))
    
    for (let i = 1; i < len; i++){
      setTimeout(arguments[i], interval);
      interval += delay * 1000;
    }    
  }
  
  function f1(){
    console.log('f1');
  }
  
  function f2(){
    console.log('f2');
  }
  
  function f3(){
    console.log('f3');
  }
  
  function f4(){
    console.log('f4');
  }
  
  function f5(){
    console.log('f5');
  }
  
  runDelayBetween (3, f1, f2, f3, f4, f5);
